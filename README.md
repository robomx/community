<div align="center">
  <br>
  <h1 align="center"><img src="https://gitlab.com/robomx/community/-/raw/29500c9b840029e87ecdcdd14d9ab970dd8f8dc0/docs/.static/community.png" width="200" height="200" />
  <p align="center" style="font-size: 50px">RoboMx</p>
  </h1>
  <p>A open community for people</p>

  <p align="center">
  <a href="https://www.ruby-lang.org/en/">
    <img src="https://img.shields.io/badge/Ruby-v2.7.1-green.svg" alt="ruby version">
  </a>
  <a href="http://rubyonrails.org/">
    <img src="https://img.shields.io/badge/Rails-v6.0.3-brightgreen.svg" alt="rails version">
  </a>
   <a href="https://travis-ci.com/RoboMx/community">
    <img src="https://travis-ci.com/RoboMx/community.svg?branch=robomx-develop" alt="rails version">
  </a>
  <a href="https://codeclimate.com/github/thepracticaldev/dev.to/maintainability">
    <img src="https://api.codeclimate.com/v1/badges/ce45bf63293073364bcb/maintainability" alt="Code Climate maintainability">
  </a>
  <a href="https://codeclimate.com/github/thepracticaldev/dev.to/test_coverage">
    <img src="https://api.codeclimate.com/v1/badges/ce45bf63293073364bcb/test_coverage" alt="Code Climate test coverage">
  </a>
  <a href="https://codeclimate.com/github/thepracticaldev/dev.to/trends/technical_debt">
    <img src="https://img.shields.io/codeclimate/tech-debt/thepracticaldev/dev.to" alt="Code Climate technical debt">
  </a>
  <a href="https://www.codetriage.com/thepracticaldev/dev.to">
    <img src="https://www.codetriage.com/thepracticaldev/dev.to/badges/users.svg" alt="CodeTriage badge">
  </a>

  <img src="https://badgen.net/dependabot/forem/forem?icon=dependabot" alt="Dependabot Badge">
  <a href="https://gitpod.io/from-referrer/">
    <img src="https://img.shields.io/badge/setup-automated-blue?logo=gitpod" alt="GitPod badge">
  </a>
  <a href="https://app.netlify.com/sites/devto/deploys">
    <img src="https://api.netlify.com/api/v1/badges/7b074d78-8fbf-46ee-915d-472cdfa7e905/deploy-status" alt="Deploy status badge">
  </a>
  <img src="https://img.shields.io/github/languages/code-size/thepracticaldev/dev.to" alt="GitHub code size in bytes">
  <img src="https://img.shields.io/github/commit-activity/w/thepracticaldev/dev.to" alt="GitHub commit activity">
  <a href="https://github.com/thepracticaldev/dev.to/issues?q=is%3Aissue+is%3Aopen+label%3A%22ready+for+dev%22">
    <img src="https://img.shields.io/github/issues/thepracticaldev/dev.to/ready for dev" alt="GitHub issues ready for dev">
  </a>
  <a href="https://app.honeybadger.io/project/Pl5JzZB5ax">
    <img src="https://img.shields.io/badge/honeybadger-active-informational" alt="Honeybadger badge">
  </a>
  <a href="https://knapsackpro.com/dashboard/organizations/1142/projects/1022/test_suites/1434/builds">
    <img src="https://img.shields.io/badge/Knapsack%20Pro-Parallel%20%2F%20dev.to-%230074ff" alt="Knapsack Pro Parallel CI builds for dev.to" style="max-width:100%;">
  </a>
</p>

</div>

Welcome to the **RoboMx Community** built using [Forem](https://forem.com)
codebase, the platform that powers [dev.to](https://dev.to). We are so excited
to have you. With your help, we can build out Forem’s usability, scalability,
and stability to better serve our communities.

## What is RoboMx Community?

A community that bring people for general discussions having a particular
characteristic in common. We here bring the best discussion topics for people to
talk, learn, educate, explore, analyze and evolve!

People who write articles, take part in discussions, and build their
professional profiles. We value supportive and constructive dialogue in the
pursuit of great code and career growth for all members. The ecosystem spans
from beginner to advanced developers, and all are welcome to find their place
within our community. ❤️

## Contributing

**Coming together is a beginning; keeping together is progress; working together
is success.** We encourage you to contribute to RoboMx Community!

## Getting Started

Join any discussion by entering the hashtags topics of your choice or create
your own discussions. Make your professional profile and write articles, blogs
and have discussions.
<div align="center">
  <strong>Keep discussing</strong>
  <br/> <br/>
<img src="https://gitlab.com/robomx/community/-/raw/99f1e73010c489cdd48b96a64506058e23facff5/community.gif" width="20" height="20" alt="commmunity-gif" />
</div>
