# `How to deploy using nginx + service file`

- Install Nginx

  `sudo apt get install nginx`

- Configure nginx server

  `sudo systemctl enable nginx`

- Create site-enabled file at `/etc/nginx/sites-available/robomx`

- Create symlink

  `sudo ln -s /etc/nginx/sites-available/robomx/ /etc/nginx/sites-enabled/robomx`

- Test server

  `sudo nginx -t`

- Restart server

  `sudo service nginx restart`

- Create a service file

  `nano /etc/systemd/system/robomx.service`

- Enable the service

  `sudo systemctl enable robomx`

- Start the server

  `sudo systemctl restart robomx`

- You can check status

  `sudo systemctl status robomx`

- Start ports

  `sudo ufw allow 'Nginx Full'`

- Add a SSL certificate using Certbot

  `sudo add-apt-repository ppa:certbot/certbot`

  `sudo apt install python-certbot-nginx`

  `sudo certbot --nginx -d domain -d www.domain`
